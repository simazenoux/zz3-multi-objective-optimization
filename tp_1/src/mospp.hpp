#ifndef MOSPP_HPP
#define MOSPP_HPP

#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <set>
#include <vector>
#include <limits>
#include <map>

std::set<std::pair<std::pair<float, float>, std::vector<int>>> mospp(std::vector<std::unordered_map<int, std::pair<float, float>>> nodes, int const origin, int const destination, int const max_size);

#endif