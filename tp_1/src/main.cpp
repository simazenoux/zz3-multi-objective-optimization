#include <array>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <array>
#include <unordered_map>

#include "mospp.hpp"


int main()
{
    std::ifstream ifstream;
    ifstream.open("../input/DLP_210.dat");
    if (ifstream.is_open())
    {
        // Parsing the file
        std::string line;

        int number_of_nodes;
        ifstream >> number_of_nodes;

        std::vector<std::unordered_map<int, std::pair<float, float>>> nodes (number_of_nodes);

        while (getline(ifstream,line) )
        {
            int from, to;
            float distance, length;
            ifstream >> from >> to >> distance >> length;
            nodes[from-1].insert({to - 1, {distance, abs(to-from) * length}});
        }
        ifstream.close();

        std::set<std::pair<std::pair<float, float>, std::vector<int>>> solutions = mospp(nodes, 0, number_of_nodes-1, 10);
        
        // Display the solution nicely
        // for (auto& solution: solutions)
        // {
        //     // std::cout << solution.first.first << "," << solution.first.second << std::endl;
        //     std::cout << "(" << solution.first.first << ", " << solution.first.second << ") :";
        //     for (int vertex: solution.second)
        //     {
        //         std::cout << " " << vertex + 1;
        //     }
        //     std::cout << std::endl;
        // }


        // Display the raw data
        // for (auto& solution: solutions)
        // {
        //     std::cout << solution.first.first << "," << solution.first.second << std::endl;
        // }

        // The line bellow avoids printing the time in the scientific notation
        std::cout << std::fixed;


        // Profiling given the maximum number of labels stored per vertex
        std::cout << "max_number_of_label, time" << std::endl;
        for (int i=50; i>= 1; i--)
        {
            const clock_t begin_time = clock();
            for (int j=0; j<10; j++)
            {
                mospp(nodes, 0, nodes.size() - 1, i);
            }
            std::cout << i << ", " << float( clock () - begin_time ) / 10 /  CLOCKS_PER_SEC << std::endl;
        }


        // Profiling given the size of the graph
        // std::cout << "graph_size, time" << std::endl;
        // while (nodes.size() >= 30)
        // {
        //     const clock_t begin_time = clock();
        //     mospp(nodes, 0, nodes.size() - 1, 100);
        //     std::cout << nodes.size() << ", " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << std::endl;

        //     // And we reduce the size of the graph by removing the last element
        //     for (auto& um: nodes)
        //     {
        //         um.erase(nodes.size() - 1);
        //     }
        //     nodes.pop_back();
        // }


        // Analysis of the result with a ± 20 % variability
        // std::cout << "distance,length,random_id" << std::endl;
        // for (int i=0; i<20; i++)
        // {
        //     std::vector<std::unordered_map<int, std::pair<float, float>>> altered_nodes = nodes;

        //     for (auto& um: altered_nodes)
        //     {
        //         for (auto& element: um)
        //         {
        //             float random = (float) (RAND_MAX - 2*rand()) / (float) RAND_MAX; // Generate a random float between -1 and 1
        //             element.second.first *= 1 + 0.2 * random;
        //             element.second.second *= 1 + 0.2 * random; 
        //         }
        //     }

        //     std::set<std::pair<std::pair<float, float>, std::vector<int>>> solutions = mospp(altered_nodes, 0, number_of_nodes-1, 100);

        //     for (auto& solution: solutions)
        //     {
        //         std::cout << solution.first.first << "," << solution.first.second << "," << i <<std::endl;
        //     }

        // }
    }
    else 
    {
        std::cout << "Unable to open file" << std::endl;
    }
  
    return EXIT_SUCCESS;
}