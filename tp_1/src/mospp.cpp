#include "mospp.hpp"

struct pair_hash 
{
    template <class T1, class T2>
    size_t operator()(const std::pair<T1, T2>& p) const
    {
        auto hash1 = std::hash<T1>{}(p.first);
        auto hash2 = std::hash<T2>{}(p.second);
 
        if (hash1 != hash2) {
            return hash1 ^ hash2;             
        }
         
        // If hash1 == hash2, their XOR is zero.
          return hash1;
    }
};

bool is_dominated(std::pair<float, float> const& solution, std::map<std::pair<float, float>, std::pair<int, std::pair<float, float>>> const & um)
{
    for (auto const&  element: um)
    {
        std::pair<float, float> label = element.first;
        if (label.first < solution.first && label.second <= solution.second
        || label.first <= solution.first && label.second < solution.second)
        {
            return true;
        }
    }
    return false;
}

void remove_newly_dominated_labels(std::pair<float, float> const& new_label, std::map<std::pair<float, float>, std::pair<int, std::pair<float, float>>> & um)
{
    std::set<std::pair<float, float>> labels_to_remove;

    for (auto& element: um)
    {
        std::pair<float, float> label = element.first;

        // If the new label dominates one of the old one
        if (label.first > new_label.first && label.second >= new_label.second
        || label.first >= new_label.first && label.second > new_label.second)
        {
            // We add the dominated label to the labels to remove from the map
            labels_to_remove.emplace(label);
            
            // N.B : we can't use "um.erase(label)" directly bc it breaks the iterator
        }
    }

    for (auto& label: labels_to_remove)
    {
        um.erase(label);
    }
}


std::pair<float, float> get_least_interesting_label(std::vector<std::pair<float, float>> ordered_labels)
{
    float worst_score = std::numeric_limits<float>::infinity();
    int worst_score_index = 0;

    for(int i=1; i<ordered_labels.size()-1; i++)
    {
        float score_top = (ordered_labels[i-1].second - ordered_labels[i].second) / (ordered_labels[i-1].first - ordered_labels[i].first);
        float score_bottom = (ordered_labels[i].first - ordered_labels[i+1].first) / (ordered_labels[i].second - ordered_labels[i+1].second);
        
        float score = - score_top - score_bottom;
        
        if (worst_score > score)
        {
            worst_score = score;
            worst_score_index = i;
        }
    }

    return ordered_labels[worst_score_index];
}

std::set<std::pair<std::pair<float, float>, std::vector<int>>> mospp(std::vector<std::unordered_map<int, std::pair<float, float>>> nodes, int const origin, int const destination, int const max_size)
{
    std::vector<std::map<std::pair<float, float>, std::pair<int, std::pair<float, float>>>> objectives (nodes.size());

    std::set<int> vertexes_to_explore;
    objectives[origin].insert({{0,0}, {-1, {0, 0}}});
    vertexes_to_explore.insert(origin);


    while (vertexes_to_explore.size())
    {
        // We take an vertex to explore
        int const current = *vertexes_to_explore.begin();
        vertexes_to_explore.erase(current);

        // For every label in the current vertex:
        for (auto& um1: objectives[current])
        {
            std::pair<float, float> label {um1.first.first, um1.first.second};
            
            // For every edge leaving current to another vertex:
            for (auto& um2: nodes[current])
            {
                int const next = um2.first;
                std::pair<float, float> edge = um2.second;
                std::pair new_label = {label.first + edge.first, label.second + edge.second};

                // If the new label is not already in the list of labels of the next vertex
                if (objectives[next].find(new_label) == objectives[next].end())
                {
                    // And if the new label is not dominated
                    if (!is_dominated(new_label, objectives[next]))
                    {
                        // We remove the potential labels that are dominated by the new label
                        remove_newly_dominated_labels(new_label, objectives[next]);

                        // We add the new label to the map
                        std::pair key = new_label;
                        std::pair<int, std::pair<float, float>> value = {current, label};

                        if (objectives[next].size() < max_size)
                        {
                            objectives[next].emplace(new_label, value);

                            // And we add the next vertex to the set of vertices to explore
                            vertexes_to_explore.insert(next);
                        }
                        else
                        {
                            if (max_size >= 2)
                            {
                                // We create a sorted array of labels including the new one
                                std::vector<std::pair<float, float>> ordered_labels;
                                for (auto& um: objectives[next])
                                {
                                    ordered_labels.push_back(um.first);
                                }
                                ordered_labels.push_back(new_label);
                                // std::sort(ordered_labels.begin(), ordered_labels.end());

                                std::pair<float, float> worst_label = get_least_interesting_label(ordered_labels);

                                if (new_label != worst_label)
                                {
                                    objectives[next].erase(worst_label);
                                    objectives[next].emplace(new_label, value);
                                    vertexes_to_explore.insert(next);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    std::set<std::pair<std::pair<float, float>, std::vector<int>>> result;

    for (auto& um: objectives[destination])
    {
        int previous_vertex = um.second.first;
        std::pair<float, float> previous_label = um.second.second;
    
        std::vector<int> path = {destination};

        while (previous_vertex != origin)
        {
            path.push_back(previous_vertex);
            std::pair<int, std::pair<float, float>> previous = objectives[previous_vertex][previous_label];
            previous_vertex = previous.first;
            previous_label = previous.second;
        }
        path.push_back(origin);

        // We rotate the order of the vector of solutions to go from the start to the end
        // This allows to insert in the last position of the vector before and this way avoid
        // O(n²/2) complexity, given n the length of the path for the solution from origin to destination
        std::reverse(path.begin(), path.end());

        result.insert({um.first, path});
    }

    return result;
}


