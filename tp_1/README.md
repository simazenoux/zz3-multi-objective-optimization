# Evaluation and optimization of systems

## Goals

The goal of this course is to find ways to evaluate and optimize different problems of optimization with graphs.

## Instructions

1. Parse the DLP_210 file into a data structure / object

2. Write a program that resolve the multi-objective shortest path problem ()

3. Create a chart of the Pareto front obtained

4. Show the evolution of the execution time given the size of the graph

5. Optimize your algorithm by only storing a limited number of labels and show the impact on the final Pareto front

6. With a limited number of labels, create a rule to select the "good" labels in every vertex

7. Make the values on the edges varies randomly with +/-20%, and execute the program 20 times.